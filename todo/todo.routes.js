import { Router } from 'express';
import { getTodos, createTodo, updateTodo } from './todo.controller.js';

const router = Router();

router.get('/', getTodos);
router.post('/', createTodo);
router.put('/:id', updateTodo);

export { router };
