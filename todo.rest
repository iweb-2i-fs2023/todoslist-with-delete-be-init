# URL für TODO API
@todosUrl = http://localhost:3001/api/todos

# Alle TODOs abfragen
GET {{todosUrl}}

###

# Neues TODO hinzufügen
POST {{todosUrl}}
Content-Type: application/json

{
    "description": "Openair Tickets kaufen",
    "isDone": false
}

###

# Verändern von existierendem TODO
PUT {{todosUrl}}/2
Content-Type: application/json

{
    "description": "Ferien machen und relaxen",
    "isDone": true
}

###

# 404 Error: Verändern von TODO mit nicht existierender Id
PUT {{todosUrl}}/1000
Content-Type: application/json

{
    "description": "Discord Channel erstellen",
    "isDone": true
}